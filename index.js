/* const http = require('http');

const server = http.createServer((req, res) => {
    res.status = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hola mundo');

});

server.listen(3000, () => {
    console.log('server on port 3000')
}); */

const express = require('express');

const app = express();

app.get('/', (req, res) => {
    res.send('Hola mundo');
});

app.listen(3000, () => {
    console.log('server en el puerto 3000');
});